from time import monotonic, time

from concurrency.settings import number_of_tasks, crunch_difficulty, wait_length
from concurrency.test_tasks import crunch_numbers, wait_for


print('=' * 40)
print(f'{monotonic()} -- {time()} -- Starting SIMPLE.')
print('=' * 40)
start = time()

task_counter = 0

for _ in range(number_of_tasks):
    wait_for(wait_length, task_number=task_counter + 1)
    task_counter += 1
    crunch_numbers(crunch_difficulty, task_number=task_counter + 1)
    task_counter += 1

end = time()
print('=' * 40)
print(f'Run took {end - start}.')
print('=' * 40)
