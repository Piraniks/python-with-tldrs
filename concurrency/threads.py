from threading import Thread
from time import monotonic, time

from concurrency.settings import number_of_tasks, crunch_difficulty, wait_length
from concurrency.test_tasks import wait_for, crunch_numbers

print('=' * 40)
print(f'{monotonic()} -- {time()} -- Starting THREADS.')
print('=' * 40)
start = time()

threads = []

for _ in range(number_of_tasks):
    new_wait_for_task = Thread(target=wait_for, args=[wait_length],
                               kwargs={'task_number': len(threads) + 1})
    threads.append(new_wait_for_task)

    new_crunch_numbers_task = Thread(target=crunch_numbers, args=[crunch_difficulty],
                                     kwargs={'task_number': len(threads) + 1})
    threads.append(new_crunch_numbers_task)

for thread in threads:
    thread.start()

for thread in threads:
    thread.join()

end = time()
print('=' * 40)
print(f'Run took {end - start}.')
print('=' * 40)
