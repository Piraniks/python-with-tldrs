import asyncio
from time import monotonic, time

from concurrency.settings import number_of_tasks, crunch_difficulty, wait_length
from concurrency.test_tasks import async_wait_for, async_crunch_numbers

print('=' * 40)
print(f'{monotonic()} -- {time()} -- Starting ASYNCIO.')
print('=' * 40)
start = time()

async_tasks = []

for _ in range(number_of_tasks):
    new_wait_for_task = async_wait_for(wait_length, task_number=len(async_tasks) + 1)
    async_tasks.append(new_wait_for_task)

    new_crunch_numbers_task = async_crunch_numbers(crunch_difficulty, task_number=len(async_tasks) + 1)
    async_tasks.append(new_crunch_numbers_task)


async def call_tasks(tasks):
    await asyncio.gather(*tasks, return_exceptions=True)

asyncio.run(call_tasks(async_tasks))

end = time()
print('=' * 40)
print(f'Run took {end - start}.')
print('=' * 40)
