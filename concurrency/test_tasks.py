from asyncio import sleep as async_sleep
from functools import wraps
from time import monotonic, time, sleep


def log_timings(task):
    @wraps(wrapped=task)
    def wrapper(*args, task_number=None, **kwargs):
        print(f'{monotonic()} -- {time()} -- Calling task {task.__name__} ({task_number}).')
        task(*args, **kwargs)
        print(f'{monotonic()} -- {time()} -- Task {task.__name__} finished ({task_number}).')

    return wrapper


def async_log_timings(task):
    @wraps(wrapped=task)
    async def wrapper(*args, task_number=None, **kwargs):
        print(f'{monotonic()} -- {time()} -- Calling task {task.__name__} ({task_number}).')
        await task(*args, **kwargs)
        print(f'{monotonic()} -- {time()} -- Task {task.__name__} finished ({task_number}).')

    return wrapper


def _fibonacci(number):
    if number > 1:
        return _fibonacci(number=number - 1) + _fibonacci(number=number - 2)
    return number


@log_timings
def crunch_numbers(difficulty=10):
    return _fibonacci(number=difficulty)


@log_timings
def wait_for(delay_seconds=30):
    sleep(delay_seconds)


@async_log_timings
async def async_crunch_numbers(difficulty=10):
    return _fibonacci(number=difficulty)


@async_log_timings
async def async_wait_for(delay_seconds=30):
    await async_sleep(delay_seconds)
