from multiprocessing import Process
from time import monotonic, time

from concurrency.settings import number_of_tasks, crunch_difficulty, wait_length
from concurrency.test_tasks import wait_for, crunch_numbers

print('=' * 40)
print(f'{monotonic()} -- {time()} -- Starting PROCESSES.')
print('=' * 40)
start = time()

processes = []

for _ in range(number_of_tasks):
    processes.append(Process(target=crunch_numbers, args=[crunch_difficulty],
                             kwargs={'task_number': len(processes) + 1}))
    processes.append(Process(target=wait_for, args=[wait_length],
                             kwargs={'task_number': len(processes) + 1}))

for process in processes:
    process.start()

for process in processes:
    process.join()

end = time()
print('=' * 40)
print(f'Run took {end - start}.')
print('=' * 40)
